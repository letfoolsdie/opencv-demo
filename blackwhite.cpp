#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

#include <cv.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <stdio.h>
#include <unistd.h>
#include <string>

using namespace cv;
using namespace std;

struct FullImageName
{

	// FullImageName(string name, string extension) {
	// 	this->name = name;
	// 	this->extension = extension;
	// }

	string name;
	string extension;
};

void takeImageExtension(string imageName, FullImageName *full_name)
{

	size_t extPos = imageName.find_last_of('.');
	string clearImageName = imageName.substr(0, extPos);
	string extension = imageName.substr(extPos + 1);

	full_name->name = clearImageName;
	full_name->extension = extension;
}

void lomoFilter(Mat image, FullImageName *image_name)
{
	Mat new_image = Mat::zeros(image.size(), image.type());
	const float alpha = 1.33;

	/// Do the operation new_image(i,j) = alpha*image(i,j) + beta
	for (int y = 0; y < image.rows; y++)
	{
		for (int x = 0; x < image.cols; x++)
		{
			for (int c = 0; c < 3; c++)
			{
				if (c != 0)
				{
					new_image.at<Vec3b>(y, x)[c] =
							saturate_cast<uchar>(alpha * (image.at<Vec3b>(y, x)[c]));
				}
				else
				{
					new_image.at<Vec3b>(y, x)[c] = saturate_cast<uchar>(1.0 * (image.at<Vec3b>(y, x)[c]));
				}
			}
		}
	}

	string newImageName = image_name->name + "_lomo." + image_name->extension;
	cout << newImageName;

	imwrite(newImageName, new_image);
	cout << "\nImage written to file successfully\n";
}

void convertToGray(Mat image, FullImageName *image_name)
{
	Mat gray_image;

	Mat kernel =
			(Mat_<float>(3, 3)
					 << 0.333,
			 0.333, 0.333,
			 0.333, 0.333, 0.333,
			 0.333, 0.333, 0.333);

	transform(image, gray_image, kernel);

	string newImageName = image_name->name + "_gray." + image_name->extension;
	cout << newImageName;

	// cvtColor(image, gray_image, CV_BGR2GRAY);
	imwrite(newImageName, gray_image);
	cout << "\nImage written to file successfully\n";
}

void convertToSepia(Mat image, FullImageName *image_name)
{
	Mat sepia_image;

	Mat kernel =
			(Mat_<float>(3, 3)
					 << 0.272,
			 0.534, 0.131,
			 0.349, 0.686, 0.168,
			 0.393, 0.769, 0.189);

	transform(image, sepia_image, kernel);

	string newImageName = image_name->name + "_sepia." + image_name->extension;
	cout << newImageName;

	imwrite(newImageName, sepia_image);
	cout << "\nImage written to file successfully\n";
}

int main(int argc, char **argv)
{
	int makeGrayFlag = 0;
	int makeSepiaFlag = 0;
	int makeLomoFlag = 0;
	int c;

	FullImageName full_name;
	Mat image;

	string imageName = argv[1];

	while ((c = getopt(argc, argv, "gsl")) != -1)
	{
		switch (c)
		{
		case 'g':
			makeGrayFlag = 1;
			break;
		case 's':
			makeSepiaFlag = 1;
			break;
		case 'l':
			makeLomoFlag = 1;
			break;
		case '?':
			fprintf(stderr, "Unknown option `-%c'.\n", optopt);
			return 1;
		default:
			abort();
		}
	}

	takeImageExtension(imageName, &full_name);

	image = imread(imageName, 1);

	if (!image.data)
	{
		printf(" No image data \n ");
		return -1;
	}

	if (makeSepiaFlag > 0)
	{
		convertToSepia(image, &full_name);
	}
	if (makeGrayFlag > 0)
	{
		convertToGray(image, &full_name);
	}

	if (makeLomoFlag > 0)
	{
		lomoFilter(image, &full_name);
	}

	return 0;
}